import { makeStyles, withStyles } from "@material-ui/core";
import React from "react";

// for convert JSS Object to css => withStyles & makeStyles

const style = {
    sideMenu: {
        display: "flex",
        flexDirection: "column",
        position: "absolute",
        left: "0px",
        width: "320px",
        height: "100%",
        backgroundColor: "#253053",
    },
};

const SideMenu = (props) => {
    console.log(props);
    const { classes } = props;
    return (
      <div className={classes.sideMenu}>
        salam
      </div>
    )
};

export default withStyles(style)(SideMenu);
