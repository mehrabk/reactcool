import {
    AppBar,
    Badge,
    Grid,
    IconButton,
    InputBase,
    makeStyles,
    Toolbar,
} from "@material-ui/core";
import React from "react";
import NotificationsNoneIcon from "@material-ui/icons/NotificationsNone";
import ChatBubbleOutlineIcon from "@material-ui/icons/ChatBubbleOutline";
import PowerSettingsNewIcon from "@material-ui/icons/PowerSettingsNew";
import SearchIcon from "@material-ui/icons/Search";

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: "#fff",
        transform: "translateZ(0)"
    },
    searchInput: {
        opacity: "0.6",
        padding: `0px ${theme.spacing(1.5)}px`,
        fontSize: "1rem",
        width: "100%",
        "&:hover": {
            backgroundColor: "#f2f2f2",
        },
        "& .MuiSvgIcon-root": {
            marginRight: theme.spacing(1.5),
        },
        "& .MuiInputBase-input": {
            color: "red",
        },
    },
    btnRoot: {
        backgroundColor: "green",
    },
    btnLabel: {
        backgroundColor: "blue",
    },
}));

export default function Header() {
    const classes = useStyles();
    return (
        <AppBar position="static" className={classes.root}>
            <Toolbar>
                <Grid container alignItems="center">
                    <Grid item xs={12} sm={6}>
                        <InputBase
                            className={classes.searchInput}
                            placeholder="Search"
                            startAdornment={<SearchIcon fontSize="small" />}
                        />
                    </Grid>
                    <Grid item sm></Grid>
                    <Grid item>
                        <IconButton
                            classes={{
                                root: classes.btnRoot,
                                label: classes.btnLabel,
                            }}
                        >
                            <Badge badgeContent={4} color="secondary">
                                <NotificationsNoneIcon fontSize="small" />
                            </Badge>
                        </IconButton>

                        <IconButton>
                            <Badge badgeContent={3} color="primary">
                                <ChatBubbleOutlineIcon
                                    fontSize="small"
                                    color="secondary"
                                />
                            </Badge>
                        </IconButton>

                        <IconButton>
                            <PowerSettingsNewIcon />
                        </IconButton>
                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    );
}
