import {
    makeStyles,
    Paper,
    TableBody,
    TableCell,
    TableRow
} from "@material-ui/core";
import PeopleOutlineOutlinedIcon from "@material-ui/icons/PeopleOutlineOutlined";
import React, { useState } from "react";
import PageHeader from "../../components/header/PageHeader";
import useTable from "../../components/reUseable/hooks/useTable";
import * as employeeService from "../../services/employeeService";
import EmployeeForm from "./EmployeeForm";

const useStyles = makeStyles((theme) => ({
    pageContent: {
        margin: theme.spacing(5),
        padding: theme.spacing(3),
    },
}));

const headCells = [
    { id: "fullName", label: "نام و نام خانوادگی" },
    { id: "email", label: "آدرس ایمیل" },
    { id: "mobile", label: "شماره موبایل" },
    { id: "department", label: "شغل" },
];

export default function Employees() {
    const classes = useStyles();

    const [records, setRecords] = useState(employeeService.getAllEmployees());

    const {
        TblContainer,
        TblHead,
        TblPagination,
        recordsAfterPagingAndSorting,
    } = useTable(records, headCells);

    return (
        <>
            <PageHeader
                title="New Employee"
                subTitle="Form Design With Validation"
                icon={<PeopleOutlineOutlinedIcon fontSize="large" />}
            />

            <Paper className={classes.pageContent}>
                <EmployeeForm />

                <TblContainer>
                    <TblHead />
                    <TableBody>
                        {recordsAfterPagingAndSorting().map((item) => (
                            <TableRow key={item.id}>
                                <TableCell>{item.fullName}</TableCell>
                                <TableCell>{item.email}</TableCell>
                                <TableCell>{item.mobile}</TableCell>
                                <TableCell>{item.department}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </TblContainer>
                <TblPagination />
            </Paper>
        </>
    );
}
