import { createMuiTheme } from "@material-ui/core";

const CustomTheme = createMuiTheme({
    direction: "rtl",
    typography: {
        fontFamily: "IRANSans",
    },
    palette: {
        primary: {
            main: "#333996",
            light: "#3c44b126",
        },
        secondary: {
            main: "#f83245",
            light: "#f8324526",
        },
        background: {
            default: "#e0f2f1",
        },
    },
    shape: {
        borderRadius: "12px",
    },
    overrides: {
        MuiAppBar: {
            root: {
            },
        },
    },
    props: {
        MuiIconButton: {
            disableRipple: true,
        },
    },
});

export default CustomTheme;
