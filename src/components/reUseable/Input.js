import { TextField } from '@material-ui/core';
import React from 'react'

export default function Input(props) {
    const { name, label, value, onChange, error=null } = props;

    return (
                    <TextField
                        variant="outlined"
                        name={name}
                        label={label}
                        value={value}
                        onChange={onChange}
                        // error
                        // helperText="Validation Error Message"
                        {...(error && ({error: true, helperText: error}))}
                    />
    )
}
