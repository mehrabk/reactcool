import React, { useState } from "react";

// Selected ListItem
export const useCustomSelectHook = (index) => {
  const [select, setSelect] = useState(0);
  const handleSelect = (index) => {
    setSelect(index);
  };

  return [select, handleSelect];
};
