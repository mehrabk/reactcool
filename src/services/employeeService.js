export const getDepartmentCollection = [
    { id: "1", title: "توسعه دهنده" },
    { id: "2", title: "مارکتینگ" },
    { id: "3", title: "حسابداری" },
    { id: "4", title: "دیگر" },
];

// same this

// export const getDepartmentCollection = () => ([
//     {id: "1", title: "توسعه دهنده"},
//     {id: "2", title: "مارکتینگ"},
//     {id: "3", title: "حسابداری"},
//     {id: "4", title: "دیگر"}
// ]);

const KEYS = {
    employees: "employees",
    employeeId: "employeeId",
};

export function insertEmployee(data) {
    let employees = getAllEmployees();
    data["id"] = generateEmployeeId();
    employees.push(data);
    localStorage.setItem(KEYS.employees, JSON.stringify(employees));
}

export function generateEmployeeId() {
    if (localStorage.getItem(KEYS.employeeId) == null) {
        localStorage.setItem(KEYS.employeeId, "0");
    }
    var id = parseInt(localStorage.getItem(KEYS.employeeId));
    localStorage.setItem(KEYS.employeeId, (++id).toString());
    return id;
}

export function getAllEmployees() {
    if (localStorage.getItem(KEYS.employees) == null) {
        localStorage.setItem(KEYS.employees, JSON.stringify([]));
    }
    let employees = JSON.parse(localStorage.getItem(KEYS.employees));
    let deparments = getDepartmentCollection;
    return employees.map((x) => ({
        ...x,
        department: deparments[x.departmentId - 1].title,
    }));
}
