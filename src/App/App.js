import { CssBaseline } from "@material-ui/core";
import {
  jssPreset,
  makeStyles,
  StylesProvider,
  ThemeProvider
} from "@material-ui/core/styles";
import { create } from "jss";
import rtl from "jss-rtl";
import React from "react";
import CustomTheme from "../assets/CustomTheme";
import "../assets/fonts/css/fontiran.css";
import Header from "../components/header/Header";
import SideMenu from "../components/sideMenu/SideMenu";
import Employees from '../pages/employees/Employees';



const useStyles = makeStyles({
  appMain:{
    paddingRight: "320px",
    width: "100%"
  }
})

// Configure JSS
const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

function App() {
  const classes = useStyles();
  return (
    <ThemeProvider theme={CustomTheme}>
      <StylesProvider jss={jss}>   
        <SideMenu />
        <div className={classes.appMain}>
          <Header />
          <Employees />
        </div>
        <CssBaseline />
      </StylesProvider>
    </ThemeProvider>
  );
}

export default App;
