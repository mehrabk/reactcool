import {
    makeStyles,
    Table,
    TableCell,
    TableHead,
    TablePagination,
    TableRow,
} from "@material-ui/core";
import React, { useState } from "react";

const useStyles = makeStyles((theme) => ({
    label: {
        marginTop: theme.spacing(3),
        // "& thead th": {
        //     fontWeight: "600",
        //     color: theme.palette.primary.main,
        //     backgroundColor: theme.palette.primary.light
        // },
        "& .MuiTableHead-root": {
            backgroundColor: theme.palette.primary.light,
        },
        "& .MuiTableCell-head": {
            color: theme.palette.primary.main,
            fontWeight: "1000",
        },
        // "& tbody td": {
        //     fontWeight: "300"
        // },
        // "& tbody tr:hover": {
        //     backgroundColor: theme.palette.secondary.main,
        //     cursor: "pointer"
        // }
        "& .MuiTableCell-body": {
            fontWeight: "500",
        },
        "& .MuiTableRow-root:hover": {
            backgroundColor: theme.palette.secondary.light,
            cursor: "pointer",
        },
    },
}));

export default function useTable(records, headCells) {
    // ===================================================
    const classes = useStyles();

    const pages = [5, 10, 15, 20];
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(pages[page]);

    // const [order, setOrder] = useState();
    // const [orderBy, SetOrderBy] = useState();
    // ===================================================
    const TblContainer = (props) => (
        <Table className={classes.label}>{props.children}</Table>
    );

    // ===================================================
    // const handleSortRequest = (cellId) => {
    //     const isAsc = orderBy === cellId && order === "asc";
    //     setOrder(isAsc ? "desc" : "asc");
    //     SetOrderBy(cellId);
    // };

    const TblHead = () => {
        return (
            <TableHead>
                <TableRow>
                    {headCells.map((headCell) => (
                        <TableCell key={headCell.id}>
                            {/* <TableSortLabel
                                direction={
                                    orderBy === headCell.id ? order : "asc"
                                }
                                onClick={() => {
                                    handleSortRequest(headCell.id);
                                }}
                            >
                                {headCell.label}
                            </TableSortLabel> */}
                            {headCell.label}
                        </TableCell>
                    ))}
                </TableRow>
            </TableHead>
        );
    };
    // ===================================================
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
        // console.log(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        console.log(parseInt(event.target.value, 10));
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const TblPagination = () => (
        <TablePagination
            component="div"
            page={page}
            rowsPerPageOptions={pages}
            rowsPerPage={rowsPerPage}
            count={records.length}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
            style={{ direction: "ltr" }}
            labelRowsPerPage="ردیف در هر صفحه"
        />
    );
    // ===================================================

    const recordsAfterPagingAndSorting = () => {
        return records.slice(page * rowsPerPage, (page + 1) * rowsPerPage);
    };

    return {
        TblContainer,
        TblHead,
        TblPagination,
        recordsAfterPagingAndSorting,
    };
}
