import {
    FormControl,
    FormHelperText,
    InputLabel,
    MenuItem,
    Select as MuiSelect,
} from "@material-ui/core";
import { ErrorSharp } from "@material-ui/icons";
import React from "react";

export default function Select(props) {
    const { name, label, value, onChange, options, error=null } = props;
    return (
        <FormControl variant="outlined" {...(error && ({error: true}))}>
            <InputLabel>{label}</InputLabel>
            <MuiSelect
                name={name}
                label={label}
                value={value}
                onChange={onChange}
            >
                <MenuItem value="">هیچکدام</MenuItem>
                {
                    options.map(item => (
                        <MenuItem key={item.id} value={item.id}>{item.title}</MenuItem>
                    ))
                }
            </MuiSelect>
            <FormHelperText>{error}</FormHelperText>
        </FormControl>
    )
}
