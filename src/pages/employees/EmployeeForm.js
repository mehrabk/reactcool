import { Grid } from "@material-ui/core";
import React from "react";
import { Controls } from "../../components/reUseable/Controls";
import { Form, useForm } from "../../components/reUseable/hooks/useForm";

import * as employeeService from "../../services/employeeService";

const genderItems = [
    { id: "male", title: "مرد" },
    { id: "female", title: "زن" },
    { id: "other", title: "دیگر" },
];

const initialFValues = {
    id: 0,
    fullName: "",
    email: "",
    mobile: "",
    city: "",
    gender: "male",
    departmentId: "",
    hiredDate: new Date(),
    isPermanent: false,
};

export default function EmployeeForm() {
    const validate = (fieldValues = values) => {
        let temp = {...errors};

        if ("fullName" in fieldValues)
            temp.fullName = fieldValues.fullName ? "" : "این گزینه نباید خالی باشد";

        if ("email" in fieldValues)
            temp.email = /$^|.+@.+..+/.test(fieldValues.email)
                ? ""
                : "فرمت ایمیل درست نیست";

        if ("mobile" in fieldValues)
            temp.mobile =
            fieldValues.mobile.length > 9 ? "" : "کمتر از ۱۰ رقم مجاز نیست";

        if ("departmentId" in fieldValues)
            temp.departmentId =
            fieldValues.departmentId.length != 0 ? "" : "این گزینه باید انتخاب گردد";

        setErrors({
            ...temp,
        });

        if (fieldValues == values) {
            return Object.values(temp).every((x) => x == "");
        }
    };

    const [
        values,
        setValues,
        errors,
        setErrors,
        handleInputChange,
        resetForm,
    ] = useForm(initialFValues, true, validate);

    const handleSubmit = (e) => {
        e.preventDefault();
        if (validate()) {
            employeeService.insertEmployee(values);
            // window.alert(values.fullName)
            resetForm();
        }
    };

    return (
        <Form onSubmit={handleSubmit}>
            <Grid container>
                <Grid item xs={6}>
                    <Controls.Input
                        name="fullName"
                        label="نام و نام خانوادگی"
                        value={values.fullName}
                        onChange={handleInputChange}
                        error={errors.fullName}
                    />

                    <Controls.Input
                        name="email"
                        label="ایمیل"
                        value={values.email}
                        onChange={handleInputChange}
                        error={errors.email}
                    />

                    <Controls.Input
                        name="mobile"
                        label="موبایل"
                        value={values.mobile}
                        onChange={handleInputChange}
                        error={errors.mobile}
                    />

                    <Controls.Input
                        name="city"
                        label="شهر"
                        value={values.city}
                        onChange={handleInputChange}
                        error={errors.city}
                    />
                </Grid>
                <Grid item xs={6}>
                    <Controls.RadioGroup
                        name="gender"
                        label="جنسیت"
                        value={values.gender}
                        onChange={handleInputChange}
                        items={genderItems}
                    />
                    <Controls.Select
                        name="departmentId"
                        label="حوزه فعالیت"
                        value={values.departmentId}
                        onChange={handleInputChange}
                        options={employeeService.getDepartmentCollection}
                        error={errors.departmentId}
                    />

                    <Controls.DatePicker
                        name="hiredDate"
                        value={values.hiredDate}
                        label="تاریخ ثبت"
                        onChange={handleInputChange}
                    />

                    <Controls.CheckBox
                        name="isPermanent"
                        label="ماندگاری"
                        value={values.isPermanent}
                        onChange={handleInputChange}
                    />

                    <div>
                        <Controls.Button text="ثبت" type="submit" />
                        <Controls.Button
                            text="ریست"
                            color="default"
                            onClick={resetForm}
                        />
                    </div>
                </Grid>
            </Grid>
        </Form>
    );
}
